﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class Player : MonoBehaviour
{
    public List<GameObject> changeBases;
    public GameObject endPoint;
    public PlayerData data;
    public string startTag;

	
	void Start ()
	{
		data = new PlayerData(1,1,1,1);//сделать загрузку пользовательских данных из json файла.
	    Base[] bases = FindObjectsOfType<Base>();
	    foreach (Base myBase in bases)
	    {
	        if (myBase.gameObject.tag == startTag)
	            myBase.data = data;
	    }

	}
	
	
	void Update ()
	{
#if UNITY_ANDROID
	    if (Input.toucnCount > 0)
	    {
	        var touchPosition = Camera.main.ScreenToWorldPoint(Input.touches[0].Position);
	        Collider2D col = Physics2D.Raycast(touchPosition, transform.position).collider;
	        AddBase(col);   
	    }
#endif
#if UNITY_EDITOR
	    if (Input.GetMouseButton(0))
	    {
	        var touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	        Collider2D col = Physics2D.Raycast(touchPosition, transform.position).collider;
	        AddBase(col);
	    }
#endif
        else if (changeBases.Count != 0)
	    {
	        foreach (GameObject myBase in changeBases)
	        {
	            Base bs = myBase.GetComponent<Base>();
	            if (bs.data == data)
	            {
	                bs.SendUnitns(endPoint);
	            }
	        }
            ClearList();
	    }
	}

    void AddBase(Collider2D col)
    {
        if (col != null)
        {
            var obj = col.gameObject;
            var bs = obj.GetComponent<Base>();
            if (bs != null)
            {
                if ((bs.data == data) && (!changeBases.Contains(obj)))
                {
                    changeBases.Add(obj);
                }

                endPoint = obj;

                foreach (GameObject myBase in changeBases)
                {
                    LineRenderer lr = myBase.GetComponent<LineRenderer>();
                    if (bs.data == data)
                    {
                        lr.SetPosition(0,myBase.transform.position);
                        lr.SetPosition(1,endPoint.transform.position);
                    }
                    else
                    {
                        lr.SetPosition(1,myBase.transform.position);
                    }
                }
            }
        }
    }

    void ClearList()
    {
        foreach (GameObject myBase in changeBases)
        {
            LineRenderer lr = myBase.GetComponent<LineRenderer>();
            lr.SetPosition(1, myBase.transform.position);
        }
        changeBases.Clear();
        endPoint = null;
    }
}

[System.Serializable]
public class PlayerData
{
    public float attack;
    public float defense;
    public float speed;
    public float reproductionTime;

    public PlayerData(float attack, float defense, float speed, float reproductionTime)
    {
        this.attack = attack;
        this.defense = defense;
        this.speed = speed;
        this.reproductionTime = reproductionTime;
    }
}
