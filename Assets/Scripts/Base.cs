﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using NUnit.Framework;
using UnityEngine;

public class Base : MonoBehaviour
{

    public PlayerData data;
    public PlayerData nullData = new PlayerData(1, 1, 1, 0);
    public GameObject Unit;
    public float mass;
    public float randomRegion;

    void Start()
    {
        if ((mass == 0) || (tag == "Untagged"))
        {
            data = nullData;
            GetComponent<SpriteRenderer>().color = Color.gray;
        }
    }

	public void SendUnitns (GameObject endPoint)
	{
	    if (endPoint != gameObject)
	    {
	        GameObject newUnit;
	        for (int i = 0; i < (int)mass / 2; i++)
	        {
	            newUnit = Instantiate(Unit, transform.position + new Vector3(Random.Range(-randomRegion, randomRegion),
	                                            Random.Range(-randomRegion, randomRegion)), Quaternion.identity);
	            var unitScript = newUnit.GetComponent<Unit>();
                unitScript.SetTarget(endPoint);
	            unitScript.data = data;
	            newUnit.GetComponent<SpriteRenderer>().color = GetComponent<SpriteRenderer>().color;
	        }
	        mass -= ((int) mass) / 2;

	    }
	}

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Unit")
        {
            var unitScript = col.gameObject.GetComponent<Unit>();
            if ((unitScript.endPoint == gameObject)&&(unitScript.data == data))
            {
                mass++;
                Destroy(col.gameObject);
            }
            else if (unitScript.data != data)
            {
                float attack = unitScript.data.attack / data.defense;
                mass -= attack;
                if (Mathf.RoundToInt(mass) == 0)
                {
                    data = nullData;
                    GetComponent<SpriteRenderer>().color = Color.gray;
                }
                else if (Mathf.RoundToInt(mass) < 0)
                {
                    data = unitScript.data;
                    GetComponent<SpriteRenderer>().color = col.gameObject.GetComponent<SpriteRenderer>().color;
                    mass = -mass;
                }
                Destroy(col.gameObject);
            }
        }
    }
	
	
	void Update () {
		
	}
}
