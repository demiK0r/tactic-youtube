﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AI : MonoBehaviour
{
    public PlayerData data;
    public float analyzingTime;
    public string startTag;

    void Start () {
        Base[] bases = FindObjectsOfType<Base>();
        foreach (Base myBase in bases)
        {
            if (myBase.gameObject.tag == startTag)
                myBase.data = data;
        }

        StartCoroutine(Analyze());
    }
	
	void Update () {
		
	}

    IEnumerator Analyze()
    {
        while (true)
        {
            if (analyzingTime > 0)
                yield return new WaitForSeconds(analyzingTime);
            else
                yield return new WaitForSeconds(4);
            Base[] bases = FindObjectsOfType<Base>();
            var myBases = bases.Where(myBase => myBase.data == data);
            bases = bases.Where(enemyBase => enemyBase.data != data).ToArray();
            var total = TotalWeight(bases);
            var target = bases.Where(enemyBase => enemyBase.mass < total / 3).Min();
            if (target != null)
            {
                foreach (Base myBase in myBases)
                {
                    myBase.SendUnitns(target.gameObject);
                }
            }

        }
    }

    float TotalWeight(Base[] bases)
    {
        float total = 0;
        foreach (Base myBase in bases)
        {
            total += myBase.mass;
        }

        return total;
    }
}
