﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public PlayerData data;
    public GameObject endPoint;
    private Vector2 endPosition;

    public void SetTarget(GameObject endPoint)
    {
        this.endPoint = endPoint;
        endPosition = endPoint.transform.position - transform.position;
        endPosition = endPosition.normalized;
    }

    void FixedUpdate ()
    {
		transform.Translate(endPosition * data.speed/40); //это заглушка, нужно исправить
	}
}
